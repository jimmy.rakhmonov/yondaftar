import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:container_tab_indicator/container_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/components/card_widget.dart';

import 'package:yondaftar/api/repository/local/database_connection.dart';
import 'package:yondaftar/components/empty_items.dart';
import 'package:yondaftar/pages/litle%20pages/PostPage.dart';

import 'litle pages/PdfViewPage.dart';

class Saved extends StatefulWidget {
  @override
  _SavedState createState() => _SavedState();
}

class _SavedState extends State<Saved> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Container(
      margin: const EdgeInsets.only(top: 20.0),
      child: DefaultTabController(
          length: 2,
          child: Column(children: [
            SizedBox(
              height: 56,
              child: TabBar(
                tabs: [
                  Text('Postlar', style: TextStyle(color: Colors.black)),
                  Text('Kitoblar', style: TextStyle(color: Colors.black)),
                ],
                indicator: ContainerTabIndicator(
                  width: 96,
                  height: 32,
                  radius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    bottomRight: Radius.circular(8.0),
                  ),
                  color: Colors.deepPurple[50],
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height / 1.181,
              child: TabBarView(
                children: [
                  Container(
                    child: FutureBuilder<List<Posts>>(
                        future: DBProvider.db.getAllPosts(),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<Posts>> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                              return Center(child: CircularProgressIndicator());
                            default:
                              if (snapshot.hasError) {
                                return Image.asset('assets/images/empty.png');
                              } else {
                                return (!snapshot.hasData)
                                    ? Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          AutoSizeText(
                                            'Hozirgi vaqtda saqlanmalar mavjud emas',
                                            minFontSize: 16,
                                            textAlign: TextAlign.center,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Image.asset(
                                              'assets/images/empty.png'),
                                        ],
                                      )
                                    : ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        itemCount: snapshot.data.length,
                                        shrinkWrap: true,
                                        itemBuilder:
                                            (BuildContext context, index) {
                                          Posts posts =
                                              snapshot.data[index] as Posts;

                                          return GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          PostPage(
                                                              posts: posts)));
                                            },
                                            child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                child: Post_Card(posts: posts)),
                                          );
                                        },
                                      );
                              }
                          }
                        }),
                  ),
                  Container(
                      child: FutureBuilder<List<Kitob>>(
                          future: DBProvider.db.getAllClients(),
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Kitob>> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                                return Center(
                                    child: Column(children: [EmptyItems()]));
                              default:
                                if (snapshot.hasError) {
                                  return Container();
                                } else {
                                  return GridView.builder(
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      crossAxisSpacing: 0,
                                      mainAxisSpacing: 0,
                                      childAspectRatio: 0.6,
                                    ),
                                    padding: EdgeInsets.all(10.0),
                                    itemCount: snapshot.data.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      Kitob kitob = snapshot.data[index];
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PdfViewPage(
                                                          kitob: kitob)));
                                        },
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              3.0,
                                          height: 300,
                                          child: Card(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.0)),
                                            elevation: 3.0,
                                            child: Column(
                                              children: <Widget>[
                                                Stack(
                                                  children: <Widget>[
                                                    Container(
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height /
                                                              3.5,
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  10),
                                                          topRight:
                                                              Radius.circular(
                                                                  10),
                                                        ),
                                                        child:
                                                            CachedNetworkImage(
                                                          imageUrl:
                                                              "${kitob.pdfPreview}",
                                                          progressIndicatorBuilder: (context,
                                                                  url,
                                                                  downloadProgress) =>
                                                              CircularProgressIndicator(
                                                                  value: downloadProgress
                                                                      .progress),
                                                          errorWidget: (context,
                                                                  url, error) =>
                                                              Icon(Icons.error),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(height: 7.0),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 15.0),
                                                  child: Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    child: Text(
                                                      "${kitob.sinfTitle}",
                                                      style: TextStyle(
                                                        fontSize: 10.0,
                                                        fontWeight:
                                                            FontWeight.w300,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: 7.0),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 15.0),
                                                  child: Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    child: AutoSizeText(
                                                      "${kitob.title}",
                                                      minFontSize: 14,
                                                      maxFontSize: 16,
                                                      style: TextStyle(
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign: TextAlign.left,
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(height: 7.0),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                }
                            }
                          })),
                ],
              ),
            )
          ])),
    );
  }
}
