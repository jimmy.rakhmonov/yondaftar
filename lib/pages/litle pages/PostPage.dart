import 'dart:developer';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/api/repository/local/database_connection.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class PostPage extends StatefulWidget {
  final Posts posts;

  PostPage({Key key, @required this.posts}) : super(key: key);

  @override
  _PostPageState createState() => _PostPageState(posts: posts);
}

class _PostPageState extends State<PostPage> {
  final Posts posts;

  _PostPageState({this.posts});

  Future<void> share() async {
    await FlutterShare.share(
        title: '${posts.title}',
        text: '${posts.title}',
        linkUrl:
            'https://play.google.com/store/apps/details?id=com.daftar.yondaftar&hl=ru&gl=US',
        chooserTitle: '${posts.title}');
  }

  YoutubePlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: '${posts.video}',
      params: const YoutubePlayerParams(
        startAt: const Duration(minutes: 0, seconds: 0),
        showControls: true,
        showFullscreenButton: true,
        desktopMode: true,
        privacyEnhanced: true,
      ),
    );
    _controller.onEnterFullscreen = () {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
      log('Entered Fullscreen');
    };
    _controller.onExitFullscreen = () {
      log('Exited Fullscreen');
    };
  }

  ScrollController mainController = ScrollController();

  @override
  Widget build(BuildContext context) {
    const player = YoutubePlayerIFrame();
    return YoutubePlayerControllerProvider(
        // Passing controller to widgets below.
        controller: _controller,
        child: SafeArea(
          child: Scaffold(
              body: Column(
            children: [
              SizedBox(
                height: 8,
              ),
              Container(
                  color: Color(0xFFf4f8fb),
                  child: Column(
                    children: [
                      AutoSizeText(
                        posts.title,
                        textAlign: TextAlign.center,
                        maxLines: 3,
                        style: TextStyle(
                            color: Color(0xFFe6683c),
                            fontSize: 18,
                            fontWeight: FontWeight.w700),
                      ),
                      SizedBox(height: 10.0),
                      Divider(
                        height: 1,
                        thickness: 1,
                        indent: 20,
                        endIndent: 20,
                      ),
                      SizedBox(height: 8.0),
                      _ActionsRow(post: posts),
                    ],
                  )),
              SizedBox(height: 8.0),
              player,
              Expanded(
                child: ListView(
                  padding: EdgeInsets.all(5.0),
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        ListTile(
                          leading: CircleAvatar(
                            child: GestureDetector(
                              onTap: () {},
                              child: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(500 / 2)),
                                child: SizedBox(
                                  height: 40,
                                  child: CachedNetworkImage(
                                      fit: BoxFit.cover,
                                      imageUrl:
                                          "https://yondaftar.uz/image/${posts.markazTitle}/${posts.markazLogo}"),
                                ),
                              ),
                            ),
                          ),
                          title: GestureDetector(
                            onTap: () {},
                            child: AutoSizeText(
                              "${posts.markazTitle} • ${posts.fanTitle} • ${posts.sinfTitle}",
                              minFontSize: 14,
                              maxFontSize: 20,
                              style: TextStyle(
                                fontSize: 8.0,
                                fontWeight: FontWeight.w800,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Divider(
                          height: 1,
                          thickness: 1,
                          indent: 15,
                          endIndent: 15,
                        ),
                        /*Html(data: posts.text),*/
                      ],
                    ),
                  ],
                ),
              ),
            ],
          )),
        ));
  }
}

class _ActionsRow extends StatelessWidget {
  final Posts post;

  const _ActionsRow({
    Key key,
    @required this.post,
  }) : super(key: key);

  Future<void> share() async {
    await FlutterShare.share(
        title: '${post.title}',
        text: '${post.fanTitle} • ${post.sinfTitle} • ${post.title}',
        linkUrl: 'https://yondaftar.uz/post?id=${post.id}',
        chooserTitle: '${post.title} bilan ulashish');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        SizedBox(
          height: 30,
          width: 30,
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(60),
            ),
            padding: EdgeInsets.zero,
            onPressed: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios_new_outlined),
            height: 30,
          ),
        ),
        _buildAction(context, Icons.remove_red_eye_outlined, post.views),
        _buildAction(context, Icons.date_range_outlined, post.pubdate),
        _buildLikeAction(context, Icons.bookmark_border_outlined, 'Saqlash'),
        _buildForwardAction(context, Icons.share, 'Ulashish'),
      ],
    );
  }

  Widget _buildAction(BuildContext context, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(icon),
        const SizedBox(height: 4.0),
        Text(
          label,
          style:
              Theme.of(context).textTheme.caption.copyWith(color: Colors.black),
        ),
      ],
    );
  }

  Widget _buildForwardAction(
      BuildContext context, IconData icon, String label) {
    return InkWell(
      onTap: () {
        share();
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(icon),
          const SizedBox(height: 4.0),
          Text(
            label,
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: Colors.black),
          ),
        ],
      ),
    );
  }

  Widget _buildLikeAction(BuildContext context, IconData icon, String label) {
    bool isFavorite = false;
    return InkWell(
      onTap: () async {
        if (await DBProvider.db.getPost(post.id) != null) {
          await DBProvider.db.deletePost(post.id);
          Fluttertoast.showToast(
              msg: "Saqlanma o`chirildi",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 10.0);
        } else {
          await DBProvider.db.newFavPost(post);
          Fluttertoast.showToast(
              msg: "Saqlanma qo`shildi",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 10.0);
        }
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(isFavorite
              ? Icons.bookmark_added_outlined
              : Icons.bookmark_add_outlined),
          const SizedBox(height: 4.0),
          Text(
            label,
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: Colors.black),
          ),
        ],
      ),
    );
  }
}
