import 'dart:async';

import 'package:bottom_sheet/bottom_sheet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/api/repository/http_client.dart';
import 'package:yondaftar/components/book_widget.dart';
import 'package:yondaftar/pages/litle pages/PdfViewPage.dart';

import 'PdfViewPage.dart';

class QidiruvSearch extends StatefulWidget {
  QidiruvSearch() : super();

  final String title = "Kitob Qidirish";

  @override
  QidiruvSearchState createState() => QidiruvSearchState();
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class QidiruvSearchState extends State<QidiruvSearch> {
  final _debouncer = Debouncer(milliseconds: 500);
  List<Kitob> books;
  List<Kitob> filteredBooks;

  @override
  void initState() {
    super.initState();
    Services2.getUsers.then((usersFromServer) {
      setState(() {
        books = usersFromServer;
        filteredBooks = books;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.black),
        title: TextField(
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(15.0),
              hintText: 'Kitob nomini kiriting',
              focusColor: Colors.black,
              hoverColor: Colors.black,
              fillColor: Colors.black),
          cursorColor: Colors.grey,
          onChanged: (string) {
            _debouncer.run(() {
              setState(() {
                filteredBooks = books
                    .where((u) => (u.title
                            .toLowerCase()
                            .contains(string.toLowerCase()) ||
                        u.title.toLowerCase().contains(string.toLowerCase())))
                    .toList();
                filteredBooks = books
                    .where((u) => (u.sinfTitle
                            .toLowerCase()
                            .contains(string.toLowerCase()) ||
                        u.title.toLowerCase().contains(string.toLowerCase())))
                    .toList();
              });
            });
          },
        ),
        backgroundColor: Colors.deepPurple[50],
        actions: [
          IconButton(
              icon: Icon(Icons.filter_alt_outlined),
              color: Colors.black,
              onPressed: (_showSheetWithoutList))
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
              child: filteredBooks?.isNotEmpty == true
                  ? GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 0,
                        childAspectRatio: 0.58,
                      ),
                      padding: EdgeInsets.all(10.0),
                      itemCount: filteredBooks.length,
                      itemBuilder: (BuildContext context, int index) {
                        final kitob = filteredBooks[index];
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        PdfViewPage(kitob: kitob)));
                          },
                          child: Container(
                            child: Book_Item(kitob: kitob),
                          ),
                        );
                      },
                    )
                  : Container(child: Center(child: CircularProgressIndicator(),),))
        ],
      ),
    );
  }

  void _showSheetWithoutList() {
    showStickyFlexibleBottomSheet<void>(
      minHeight: 0,
      initHeight: 0.5,
      maxHeight: .8,
      headerHeight: 80,
      context: context,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40.0),
          topRight: Radius.circular(40.0),
        ),
      ),
      headerBuilder: (context, offset) {
        return AnimatedContainer(
          duration: const Duration(milliseconds: 300),
          width: double.infinity,
          height: 200,
          decoration: BoxDecoration(
            color: Colors.deepPurple[50],
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(offset == 0.8 ? 0 : 40),
              topRight: Radius.circular(offset == 0.8 ? 0 : 40),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Center(
                  child: Text(
                    'Kategoriyalarni tanlang',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
              ),
            ],
          ),
        );
      },
      builder: (context, offset) {
        return SliverChildListDelegate(
          _getChildren(offset, isShowPosition: false),
        );
      },
      anchors: [.2, 0.5, .8],
    );
  }

  List<Widget> _getChildren(double bottomSheetOffset, {bool isShowPosition}) =>
      <Widget>[
        if (isShowPosition)
          Text(
            'position $bottomSheetOffset',
            style: Theme.of(context).textTheme.headline6,
          ),
      ];
}
