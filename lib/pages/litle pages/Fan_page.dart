import 'package:container_tab_indicator/container_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/api/repository/http_client.dart';
import 'package:yondaftar/components/book_widget.dart';
import 'package:yondaftar/components/card_widget.dart';
import 'package:yondaftar/components/empty_items.dart';
import 'package:yondaftar/pages/home/components/section_title.dart';

class FanPage extends StatefulWidget {
  final Fanlar fan;

  FanPage({Key key, @required this.fan}) : super(key: key);

  @override
  _FanPageState createState() => _FanPageState(fan);
}

class _FanPageState extends State<FanPage> {
  List<Kitob> filtered1Books;
  List<Kitob> filtered2Books;
  List<Kitob> filtered3Books;
  List<Kitob> filtered4Books;
  List<Kitob> filtered5Books;
  List<Kitob> filtered6Books;
  List<Kitob> filtered7Books;
  List<Kitob> filtered8Books;
  List<Kitob> books;
  List<Posts> posts;
  List<Posts> filtered1Posts;
  List<Posts> filtered2Posts;
  List<Posts> filtered3Posts;
  List<Posts> filtered4Posts;
  List<Posts> filtered5Posts;
  List<Posts> filtered6Posts;
  List<Posts> filtered7Posts;
  List<Posts> filtered8Posts;
  List<Markaz> markaz;
  List<Markaz> filteredMarkaz;
  List<Sinf> sinf;
  List<Sinf> filteredSinf;

  final Fanlar fan;

  _FanPageState(this.fan);

  @override
  void initState() {
    super.initState();
    Services2.fetchSinf.then((sinfFromServer) {
      setState(() {
        sinf = sinfFromServer;
        filteredSinf = sinf;
      });
    });
    Services2.getFilteredPosts("1", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered1Posts = posts;
      });
    });
    Services2.getSecondFilteredPosts("2", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered2Posts = posts;
      });
    });
    Services2.getFiltered3Posts("3", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered3Posts = posts;
      });
    });
    Services2.getFiltered4Posts("4", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered4Posts = posts;
      });
    });
    Services2.getFiltered5Posts("5", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered5Posts = posts;
      });
    });
    Services2.getFiltered6Posts("6", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered6Posts = posts;
      });
    });
    Services2.getFiltered7Posts("7", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered7Posts = posts;
      });
    });
    Services2.getFiltered8Posts("8", fan.id).then((filterServer) {
      setState(() {
        posts = filterServer;
        filtered8Posts = posts;
      });
    });

    Services2.getFilteredBooks("1", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered1Books = books;
      });
    });

    Services2.getFiltered2Books("2", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered2Books = books;
      });
    });

    Services2.getFiltered3Books("3", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered3Books = books;
      });
    });

    Services2.getFiltered4Books("4", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered4Books = books;
      });
    });

    Services2.getFiltered5Books("5", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered5Books = books;
      });
    });

    Services2.getFiltered6Books("6", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered6Books = books;
      });
    });

    Services2.getFiltered7Books("7", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered7Books = books;
      });
    });

    Services2.getFiltered8Books("8", fan.id).then((filterServer) {
      setState(() {
        books = filterServer;
        filtered8Books = books;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          title: Text(
            "${fan.title}",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          backgroundColor: Colors.deepPurple[50],
        ),
        body: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Padding(
            padding: EdgeInsets.only(top: 10),
            child: Column(
              children: [
                DefaultTabController(
                    length: 8,
                    child: Column(children: [
                      SizedBox(
                        height: 56,
                        child: TabBar(
                          isScrollable: true,
                          tabs: [
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text("5 Sinf",
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text('6 Sinf',
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text("7 Sinf",
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text('8 Sinf',
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text("9 Sinf",
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text('10 Sinf',
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text("11 Sinf",
                                    style: TextStyle(color: Colors.black))),
                            Padding(
                                padding: EdgeInsets.all(20),
                                child: Text('Kurs +',
                                    style: TextStyle(color: Colors.black))),
                          ],
                          indicator: ContainerTabIndicator(
                            width: 96,
                            height: 32,
                            radius: BorderRadius.all(Radius.circular(50 / 10)),
                            color: Colors.deepPurple[50],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 1.181,
                        child: TabBarView(
                          children: [
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child: FutureBuilder<List<Kitob>>(
                                            future: Services2.getFilteredBooks(
                                                "1", fan.id),
                                            builder: (context, snapshot) {
                                              final book = snapshot.data;

                                              switch (
                                                  snapshot.connectionState) {
                                                case ConnectionState.waiting:
                                                  return Center(
                                                      child: Column(children: [
                                                    EmptyItems()
                                                  ]));
                                                default:
                                                  if (snapshot.hasError) {
                                                    return Container();
                                                  } else {
                                                    return book.isNotEmpty == true 
                                                        ? Column(
                                                      children: [
                                                         
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 10,
                                                              left: 10,
                                                            ),
                                                            child: SectionTitle(
                                                                title:
                                                                    "Kitoblar")),
                                                        BooksList(
                                                            filteredBooks: book)
                                                      ],
                                                    ): Container();
                                                  }
                                              }
                                            },
                                          )),
                                      FutureBuilder<List<Posts>>(
                                        future: Services2.getFilteredPosts(
                                            "1", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child: Column(children: [
                                                EmptyPosts()
                                              ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ): Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child: FutureBuilder<List<Kitob>>(
                                            future: Services2.getFiltered2Books(
                                                "2", fan.id),
                                            builder: (context, snapshot) {
                                              final book = snapshot.data;

                                              switch (
                                                  snapshot.connectionState) {
                                                case ConnectionState.waiting:
                                                  return Center(
                                                      child: Column(children: [
                                                    EmptyItems()
                                                  ]));
                                                default:
                                                  if (snapshot.hasError) {
                                                    return Container();
                                                  } else {
                                                    return  book.isNotEmpty == true 
                                                        ? Column(
                                                      children: [
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 10,
                                                              left: 10,
                                                            ),
                                                            child: SectionTitle(
                                                                title:
                                                                    "Kitoblar")),
                                                        BooksList(
                                                            filteredBooks: book)
                                                      ],
                                                    ) : Container();
                                                  }
                                              }
                                            },
                                          )),
                                      FutureBuilder<List<Posts>>(
                                        future:
                                            Services2.getSecondFilteredPosts(
                                                "2", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child: Column(children: [
                                                EmptyPosts()
                                              ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child: FutureBuilder<List<Kitob>>(
                                            future: Services2.getFiltered3Books(
                                                "3", fan.id),
                                            builder: (context, snapshot) {
                                              final book = snapshot.data;

                                              switch (
                                                  snapshot.connectionState) {
                                                case ConnectionState.waiting:
                                                  return Center(
                                                      child: Column(children: [
                                                    EmptyItems()
                                                  ]));
                                                default:
                                                  if (snapshot.hasError) {
                                                    return Container();
                                                  } else {
                                                    return  book.isNotEmpty == true 
                                                        ? Column(
                                                      children: [
                                                        Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                              top: 10,
                                                              left: 10,
                                                            ),
                                                            child: SectionTitle(
                                                                title:
                                                                    "Kitoblar")),
                                                        BooksList(
                                                            filteredBooks: book)
                                                      ],
                                                    ) : Container();
                                                  }
                                              }
                                            },
                                          )),
                                      FutureBuilder<List<Posts>>(
                                        future: Services2.getFiltered3Posts(
                                            "3", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child: Column(children: [
                                                EmptyPosts()
                                              ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child:
                                              FutureBuilder<List<Kitob>>(
                                        future:
                                            Services2.getFiltered4Books("4", fan.id),
                                        builder: (context, snapshot) {
                                          final book = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyItems()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Container();
                                              } else {
                                                return  book.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Kitoblar")),
                                                    BooksList(filteredBooks: book)
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )),
                                      FutureBuilder<List<Posts>>(
                                        future:
                                            Services2.getFiltered4Posts(
                                                "4", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyPosts()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child:
                                              FutureBuilder<List<Kitob>>(
                                        future:
                                            Services2.getFiltered5Books("5", fan.id),
                                        builder: (context, snapshot) {
                                          final book = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyItems()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Container();
                                              } else {
                                                return  book.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Kitoblar")),
                                                    BooksList(filteredBooks: book)
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )),
                                      FutureBuilder<List<Posts>>(
                                        future:
                                            Services2.getFiltered5Posts(
                                                "5", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyPosts()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child:
                                              FutureBuilder<List<Kitob>>(
                                        future:
                                            Services2.getFiltered6Books("6", fan.id),
                                        builder: (context, snapshot) {
                                          final book = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyItems()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Container();
                                              } else {
                                                return  book.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Kitoblar")),
                                                    BooksList(filteredBooks: book)
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )),
                                      FutureBuilder<List<Posts>>(
                                        future:
                                            Services2.getFiltered6Posts(
                                                "6", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyPosts()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child:
                                              FutureBuilder<List<Kitob>>(
                                        future:
                                            Services2.getFiltered7Books("7", fan.id),
                                        builder: (context, snapshot) {
                                          final book = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyItems()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Container();
                                              } else {
                                                return  book.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Kitoblar")),
                                                    BooksList(filteredBooks: book)
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )),
                                      FutureBuilder<List<Posts>>(
                                        future:
                                            Services2.getFiltered7Posts(
                                                "7", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyPosts()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0))),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                          ),
                                          child:
                                              FutureBuilder<List<Kitob>>(
                                        future:
                                            Services2.getFiltered8Books("8", fan.id),
                                        builder: (context, snapshot) {
                                          final book = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyItems()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Container();
                                              } else {
                                                return  book.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Kitoblar")),
                                                    BooksList(filteredBooks: book)
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      )),
                                      FutureBuilder<List<Posts>>(
                                        future:
                                            Services2.getFiltered8Posts(
                                                "8", fan.id),
                                        builder: (context, snapshot) {
                                          final post = snapshot.data;

                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return Center(
                                                  child:
                                                      Column(children: [
                                                        EmptyPosts()
                                                      ]));
                                            default:
                                              if (snapshot.hasError) {
                                                return Center(
                                                  child: Column(children: [
                                                    Image.asset(
                                                        "assets/images/empty.png")
                                                  ]),
                                                );
                                              } else {
                                                return  post.isNotEmpty == true 
                                                        ? Column(
                                                  children: [
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                          top: 10,
                                                          left: 10,
                                                        ),
                                                        child: SectionTitle(
                                                            title: "Postlar")),
                                                    PostsList(
                                                        filteredPosts: post),
                                                  ],
                                                ) : Container();
                                              }
                                          }
                                        },
                                      ) 
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      )
                    ])),
              ],
            ),
          ),
        ));
  }
}

class PostsList extends StatelessWidget {
  const PostsList({
    Key key,
    @required this.filteredPosts,
  }) : super(key: key);

  final List<Posts> filteredPosts;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: filteredPosts.length,
      itemBuilder: (BuildContext context, index) {
        final posts = filteredPosts[index];

        return Column(children: <Widget>[Post_Card(posts: posts)]);
      },
    );
  }
}

class BooksList extends StatelessWidget {
  const BooksList({
    Key key,
    @required this.filteredBooks,
  }) : super(key: key);

  final List<Kitob> filteredBooks;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10),
        height: 200,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: filteredBooks.length,
          itemBuilder: (BuildContext context, index) {
            final kitob = filteredBooks[index];

            return Row(children: <Widget>[
              GestureDetector(
                  onTap: () {},
                  child: Container(
                      width: MediaQuery.of(context).size.width / 3.0,
                      height: 190,
                      child: Book_Item(
                        kitob: kitob,
                      ))),
            ]);
          },
        ));
  }
}
