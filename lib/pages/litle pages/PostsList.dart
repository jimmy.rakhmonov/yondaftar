import 'dart:async';

import 'package:bottom_sheet/bottom_sheet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/api/repository/http_client.dart';
import 'package:yondaftar/components/card_widget.dart';

class QidiruvPosts extends StatefulWidget {
  QidiruvPosts() : super();

  final String title = "Video Daras Qidirish";

  @override
  QidiruvPostsState createState() => QidiruvPostsState();
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class QidiruvPostsState extends State<QidiruvPosts> {
  final _debouncer = Debouncer(milliseconds: 500);
  List<Posts> posts;
  List<Posts> filteredPosts;

  @override
  void initState() {
    super.initState();
    Services2.fetchPosts.then((usersFromServer) {
      setState(() {
        posts = usersFromServer;
        filteredPosts = posts;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.black),
        title: TextField(
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.all(15.0),
              hintText: 'Video dars nomini kiriting',
              focusColor: Colors.black,
              hoverColor: Colors.black,
              fillColor: Colors.black),
          cursorColor: Colors.grey,
          onChanged: (string) {
            _debouncer.run(() {
              setState(() {
                filteredPosts = posts
                    .where((u) => (u.title
                            .toLowerCase()
                            .contains(string.toLowerCase()) ||
                        u.title.toLowerCase().contains(string.toLowerCase())))
                    .toList();
                filteredPosts = posts
                    .where((u) => (u.sinfTitle
                            .toLowerCase()
                            .contains(string.toLowerCase()) ||
                        u.title.toLowerCase().contains(string.toLowerCase())))
                    .toList();
              });
            });
          },
        ),
        backgroundColor: Colors.deepPurple[50],
        actions: [
          IconButton(
              icon: Icon(Icons.filter_alt_outlined),
              color: Colors.black,
              onPressed: () => {})
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
              child: filteredPosts?.isNotEmpty == true
                  ? ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: filteredPosts.length,
                      itemBuilder: (BuildContext context, index) {
                        final posts = filteredPosts[index];

                        return Column(
                            children: <Widget>[Post_Card(posts: posts)]);
                      },
                    )
                  : Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ))
        ],
      ),
    );
  }
}
