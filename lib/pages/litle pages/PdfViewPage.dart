import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf_flutter/pdf_flutter.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/api/repository/local/database_connection.dart';

class PdfViewPage extends StatelessWidget {
  Kitob kitob;

  PdfViewPage({this.kitob});

  Future<String> getFilePath() async {
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
    String appDocumentsPath = appDocumentsDirectory.path;
    String filePath = '$appDocumentsPath/demoTextFile.txt';

    return filePath;
  }

  void saveFile() async {
    File file = File(await getFilePath());
    file.writeAsString("${kitob.pdf}");
  }

  void readFile() async {
    File file = File(await getFilePath());
    String fileContent = await file.readAsString();

    print('File Content: $fileContent');
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.deepPurple[50],
        leading: BackButton(color: Colors.black87),
        actions: [
          Center(
              child: _buildLikeAction(
                  context, Icons.bookmark_border_outlined, 'Saqlash')),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
          ),
        ],
        title: Text(
          '${kitob.title}' ' | ' '${kitob.sinfTitle}',
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Center(child: PDF.network("https://" + kitob.pdf)),
    );
  }

  Widget _buildLikeAction(BuildContext context, IconData icon, String label) {
    bool isFavorite = false;
    return GestureDetector(
      onTap: () async {
        if (await DBProvider.db.getPost(kitob.id) != null) {
          await DBProvider.db.deletePost(kitob.id);
          Fluttertoast.showToast(
              msg: "Saqlanma o`chirildi",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 10.0);
        } else {
          await DBProvider.db.newFav(kitob);
          Fluttertoast.showToast(
              msg: "Saqlanma qo`shildi",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 2,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 10.0);
        }
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            isFavorite
                ? Icons.bookmark_added_outlined
                : Icons.bookmark_add_outlined,
            color: Colors.black,
          ),
          const SizedBox(height: 4.0),
          Text(
            label,
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: Colors.black),
          ),
        ],
      ),
    );
  }
}
