import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/api/repository/http_client.dart';
import 'package:yondaftar/components/book_widget.dart';
import 'package:yondaftar/components/card_widget.dart';
import 'package:yondaftar/components/empty_items.dart';

import 'package:yondaftar/pages/home/components/section_title.dart';
import 'package:yondaftar/pages/litle%20pages/Fan_page.dart';
import 'package:yondaftar/pages/litle%20pages/PostsList.dart';
import 'package:yondaftar/pages/litle%20pages/book_search.dart';

import '../litle pages/PdfViewPage.dart';

class Asosiy extends StatefulWidget {
  Asosiy() : super();

  final String title = "Kitob Qidirish";

  @override
  AsosiyState createState() => AsosiyState();
}

class AsosiyState extends State<Asosiy> {
  List<Kitob> filteredBooks;
  List<Kitob> books;
  List<Posts> posts;
  List<Posts> filteredPosts;
  List<Fanlar> fan;
  List<Fanlar> filteredFan;
  List<Markaz> markaz;
  List<Markaz> filteredMarkaz;
  List<Sinf> sinf;
  List<Sinf> filteredSinf;

  @override
  void initState() {
    super.initState();
    Services2.fetchPosts.then((postsFromServer) {
      setState(() {
        posts = postsFromServer;
        filteredPosts = posts;
      });
    });
    Services2.getUsers.then((bookFromServer) {
      setState(() {
        books = bookFromServer;
        filteredBooks = books;
      });
    });
    Services2.fetchFan.then((fanFromServer) {
      setState(() {
        fan = fanFromServer;
        filteredFan = fan;
      });
    });
    Services2.fetchSinf.then((sinfFromServer) {
      setState(() {
        sinf = sinfFromServer;
        filteredSinf = sinf;
      });
    });
    Services2.fetchMarkaz.then((markazFromServer) {
      setState(() {
        markaz = markazFromServer;
        filteredMarkaz = markaz;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Scaffold(
        body: Container(
      color: Color(0xFFf4f8fb),
      child: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              top: 40,
              left: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Kitoblar',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => QidiruvSearch()));
                  },
                  child: Text('Xammasi'),
                )
              ],
            ),
          ),
          PostsListRow(filteredBooks: filteredBooks),
          Container(
            margin: EdgeInsets.only(
              top: 17,
              left: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Fa`nlar',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
          Container(
              height: 105,
              margin: const EdgeInsets.only(
                top: 20.0,
              ),
              child: filteredFan?.isNotEmpty == true
                  ? ListView.builder(
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.only(
                        left: 2.5,
                      ),
                      itemCount: filteredFan.length,
                      itemBuilder: (context, index) {
                        final fan = filteredFan[index];
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FanPage(
                                          fan: fan,
                                        )));
                          },
                          child: Container(
                            margin: const EdgeInsets.only(right: 35.0),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: 70,
                                  height: 70,
                                  margin: const EdgeInsets.only(bottom: 10.0),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5.0),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        blurRadius: 10.0,
                                        color: Colors.grey[300],
                                        offset: Offset(6.0, 6.0),
                                      )
                                    ],
                                  ),
                                  child: Column(children: [
                                    ShaderMask(
                                      blendMode: BlendMode.srcATop,
                                      shaderCallback: (bounds) =>
                                          LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                        colors: [Colors.purple, Colors.red],
                                      ).createShader(bounds),
                                      child: Icon(Icons.map_outlined),
                                    )
                                  ]),
                                ),
                                Text(
                                  filteredFan[index].title,
                                  style: TextStyle(fontSize: 17.0),
                                ),
                              ],
                            ),
                          ),
                        );
                      })
                  : Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.only(
                            left: 20.0,
                          ),
                          itemCount: 5,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: const EdgeInsets.only(right: 35.0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: 70,
                                    height: 70,
                                    margin: const EdgeInsets.only(bottom: 5.0),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5.0),
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 10.0,
                                          color: Colors.grey[300],
                                          offset: Offset(6.0, 6.0),
                                        )
                                      ],
                                    ),
                                    child: Column(children: [
                                      ShaderMask(
                                        blendMode: BlendMode.srcATop,
                                        shaderCallback: (bounds) =>
                                            LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          colors: [Colors.purple, Colors.red],
                                        ).createShader(bounds),
                                        child: Icon(Icons.map_outlined),
                                      )
                                    ]),
                                  ),
                                  Container(
                                    width: 40,
                                    height: 10,
                                  ),
                                ],
                              ),
                            );
                          }))),
          Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Video darslar',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => QidiruvPosts()));
                  },
                  child: Text('Xammasi'),
                )
              ],
            ),
          ),
          PostsList(
            filteredPosts: filteredPosts,
          ),
        ],
      )),
    ));
  }
}

class PostsList extends StatelessWidget {
  const PostsList({Key key, @required this.filteredPosts}) : super(key: key);

  final List<Posts> filteredPosts;

  @override
  Widget build(BuildContext context) {
    return filteredPosts?.isNotEmpty == true
        ? ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 15,
            itemBuilder: (BuildContext context, index) {
              final posts = filteredPosts[index];

              return Column(children: <Widget>[Post_Card(posts: posts)]);
            },
          )
        : ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 5,
            itemBuilder: (BuildContext context, index) {
              return Column(children: <Widget>[EmptyPosts()]);
            },
          );
  }
}

class PostsListRow extends StatelessWidget {
  const PostsListRow({
    Key key,
    @required this.filteredBooks,
  }) : super(key: key);

  final List<Kitob> filteredBooks;

  @override
  Widget build(BuildContext context) {
    return filteredBooks?.isNotEmpty == true
        ? Container(
            margin: EdgeInsets.only(top: 10),
            height: 200,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (BuildContext context, index) {
                final kitob = filteredBooks[index];

                return Row(children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PdfViewPage(
                                      kitob: kitob,
                                    )));
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width / 3.0,
                          height: 190,
                          child: Book_Item(
                            kitob: kitob,
                          ))),
                ]);
              },
            ))
        : Container(
            margin: EdgeInsets.only(top: 10),
            height: 200,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (BuildContext context, index) {
                return Row(children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width / 3.0,
                      height: 190,
                      child: EmptyItems())
                ]);
              },
            ));
  }
}
