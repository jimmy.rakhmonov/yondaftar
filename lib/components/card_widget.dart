import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:yondaftar/api/models/models.dart';
import 'package:yondaftar/pages/litle%20pages/PostPage.dart';
import 'package:yondaftar/repository/helpers/const.dart';
import 'package:http/http.dart' as http;

import '../api/repository/http_client.dart';


// ignore: camel_case_types
class Post_Card extends StatelessWidget {
  const Post_Card({
    Key key,
    @required this.posts,
  }) : super(key: key);

  final Posts posts;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Services2.updateView(posts.id,posts.views);
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => PostPage(posts: posts)));
      },
      child: Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        elevation: 2,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 3.5,
                  width: MediaQuery.of(context).size.width,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    child: CachedNetworkImage(
                      fit: BoxFit.cover,
                      imageUrl:
                          'https://img.youtube.com/vi/${posts.images}/mqdefault.jpg',
                      progressIndicatorBuilder:
                          (context, url, downloadProgress) =>
                              Container(
                                height: 70,
                                width: 70,
                                child: CircularProgressIndicator(
                                    value: downloadProgress.progress),
                              ),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 6.0,
                  left: 6.0,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0)),
                    child: Padding(
                      padding: EdgeInsets.all(2.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            " ${posts.sinfTitle} ",
                            style: TextStyle(
                              fontSize: 10.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 6.0,
                  right: 6.0,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0)),
                    child: Padding(
                      padding: EdgeInsets.all(2.0),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.visibility,
                            color: Constants.viewsBG,
                            size: 10.0,
                          ),
                          Text(
                            " ${posts.views} ",
                            style: TextStyle(
                              fontSize: 10.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            ListTile(
              leading: CircleAvatar(
                child: GestureDetector(
                  onTap: () {},
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(500 / 2)),
                    child: SizedBox(
                      height: 40,
                      child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl:
                              "https://yondaftar.uz/image/${posts.markazTitle}/${posts.markazLogo}"),
                    ),
                  ),
                ),
              ),
              title: GestureDetector(
                onTap: () {},
                child: AutoSizeText(
                  "${posts.title}",
                  minFontSize: 14,
                  maxFontSize: 20,
                  style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.w800,
                  ),
                  textAlign: TextAlign.left,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              subtitle: Text(
                "${posts.markazTitle} • ${posts.fanTitle} • ${posts.pubdate}",
                style: TextStyle(
                  fontSize: 10.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
