import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class EmptyPosts extends StatelessWidget {
  const EmptyPosts({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)),
      elevation: 2,
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
            ),
            child: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: Container(
                    height:
                        MediaQuery.of(context).size.height / 3.5,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.grey[300])),
          ),
          ListTile(
              leading: Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                child: CircleAvatar(
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                        Radius.circular(500 / 2)),
                    child: SizedBox(
                      height: 40,
                    ),
                  ),
                ),
              ),
              title: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                      height: 17.0, color: Colors.grey[300])),
              subtitle: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                      width: 50,
                      height: 12.0,
                      color: Colors.grey[300]))),
        ],
      ),
    );
  }
}

class EmptyItems extends StatelessWidget {
  const EmptyItems({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)),
      elevation: 3.0,
      child: Column(
        children: <Widget>[
          Container(
            height: 130,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
              child: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                      height: 130.0, color: Colors.grey[300])),
            ),
          ),
          SizedBox(height: 7.0),
          Padding(
            padding: EdgeInsets.only(right: 15.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                      height: 10.0, color: Colors.grey[300])),
            ),
          ),
          SizedBox(height: 7.0),
          Padding(
            padding: EdgeInsets.only(right: 15.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Shimmer.fromColors(
                  baseColor: Colors.grey[300],
                  highlightColor: Colors.grey[100],
                  child: Container(
                      height: 17.0, color: Colors.grey[300])),
            ),
          ),
          SizedBox(height: 7.0),
        ],
      ),
    );
  }
}
