class Category {
  final String name;
  final int numOfCourses;
  final String image;
  final String pageUrl;

  Category(this.name, this.numOfCourses, this.image, this.pageUrl);
}

List<Category> categories = categoriesData
    .map((item) =>
        Category(item['name'], item['courses'], item['image'], item['pageUrl']))
    .toList();

var categoriesData = [
  {
    "name": "Fa`nlar",
    'courses': 17,
    'image': "https://i.ibb.co/d7wtf9r/book.png"
  },
  {
    "name": "Mavzular",
    'courses': 25,
    'image': "https://i.ibb.co/d7wtf9r/book.png"
  },
  {
    "name": "Kitoblar",
    'courses': 13,
    'image': "https://i.ibb.co/d7wtf9r/book.png"
  },
  {
    "name": "Kurslar",
    'courses': 17,
    'image': "https://i.ibb.co/d7wtf9r/book.png"
  },
];
