import 'dart:convert';

List<Posts> postsFromJson(String str) => List<Posts>.from(json.decode(str).map((x) => Posts.fromMap(x)));

String postsToJson(List<Posts> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Posts {
    Posts({
        this.id,
        this.title,
        this.text,
        this.images,
        this.video,
        this.idSinf,
        this.idFan,
        this.pubdate,
        this.views,
        this.markazTitle,
        this.markazLogo,
        this.fanTitle,
        this.sinfTitle,
    });

    String id;
    String title;
    String text;
    String images;
    String video;
    String idSinf;
    String idFan;
    String pubdate;
    String views;
    String markazTitle;
    String markazLogo;
    String fanTitle;
    String sinfTitle;

    factory Posts.fromMap(Map<String, dynamic> json) => Posts(
        id: json["id"],
        title: json["title"],
        text: json["text"],
        images: json["images"],
        video: json["video"],
        idSinf: json["id_sinf"],
        idFan: json["id_fan"],
        pubdate: json["pubdate"],
        views: json["views"],
        markazTitle: json["markaz_title"],
        markazLogo: json["markaz_logo"],
        fanTitle: json["fan_title"],
        sinfTitle: json["sinf_title"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "text": text,
        "images": images,
        "video": video,
        "id_sinf": idSinf,
        "id_fan": idFan,
        "pubdate": pubdate,
        "views": views,
        "markaz_title": markazTitle,
        "markaz_logo": markazLogo,
        "fan_title": fanTitle,
        "sinf_title": sinfTitle,
    };
}





List<Fanlar> fanlarFromJson(String str) => List<Fanlar>.from(json.decode(str).map((x) => Fanlar.fromMap(x)));

String fanlarToJson(List<Fanlar> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Fanlar {
    Fanlar({
        this.id,
        this.title,
        this.idSinf,
        this.icon,
        this.icon2,
        this.image,
    });

    String id;
    String title;
    String idSinf;
    String icon;
    String icon2;
    String image;

    factory Fanlar.fromMap(Map<String, dynamic> json) => Fanlar(
        id: json["id"],
        title: json["title"],
        idSinf: json["id_sinf"],
        icon: json["icon"],
        icon2: json["icon2"],
        image: json["image"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "id_sinf": idSinf,
        "icon": icon,
        "icon2": icon2,
        "image": image,
    };
}


Kitob kitobFromMap(String str) => Kitob.fromMap(json.decode(str));

String kitobToMap(Kitob data) => json.encode(data.toMap());

class Kitob {
    Kitob({
        this.id,
        this.title,
        this.pdf,
        this.pdfPreview,
        this.fanTitle,
        this.sinfTitle,
    });

    String id;
    String title;
    String pdf;
    String pdfPreview;
    String fanTitle;
    String sinfTitle;

    factory Kitob.fromMap(Map<String, dynamic> json) => Kitob(
        id: json["id"],
        title: json["title"],
        pdf: json["pdf"],
        pdfPreview: json["pdf_preview"],
        fanTitle: json["fan_title"],
        sinfTitle: json["sinf_title"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "pdf": pdf,
        "pdf_preview": pdfPreview,
        "fan_title": fanTitle,
        "sinf_title": sinfTitle,
    };
}

List<Markaz> markazFromJson(String str) => List<Markaz>.from(json.decode(str).map((x) => Markaz.fromMap(x)));

String markazToJson(List<Markaz> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Markaz {
    Markaz({
        this.id,
        this.title,
        this.name,
        this.about,
        this.idViloyat,
        this.idShahar,
        this.idTuman,
        this.kucha,
        this.uy,
        this.telephone,
        this.telephone2,
        this.telegramm,
        this.clock,
        this.image1,
        this.image2,
        this.image3,
        this.image4,
    });

    String id;
    String title;
    String name;
    String about;
    String idViloyat;
    String idShahar;
    String idTuman;
    String kucha;
    String uy;
    String telephone;
    dynamic telephone2;
    String telegramm;
    String clock;
    String image1;
    String image2;
    String image3;
    String image4;

    factory Markaz.fromMap(Map<String, dynamic> json) => Markaz(
        id: json["id"],
        title: json["title"],
        name: json["name"],
        about: json["about"],
        idViloyat: json["id_viloyat"],
        idShahar: json["id_shahar"],
        idTuman: json["id_tuman"],
        kucha: json["kucha"],
        uy: json["uy"],
        telephone: json["telephone"],
        telephone2: json["telephone2"],
        telegramm: json["telegramm"],
        clock: json["clock"],
        image1: json["image1"],
        image2: json["image2"],
        image3: json["image3"],
        image4: json["image4"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "name": name,
        "about": about,
        "id_viloyat": idViloyat,
        "id_shahar": idShahar,
        "id_tuman": idTuman,
        "kucha": kucha,
        "uy": uy,
        "telephone": telephone,
        "telephone2": telephone2,
        "telegramm": telegramm,
        "clock": clock,
        "image1": image1,
        "image2": image2,
        "image3": image3,
        "image4": image4,
    };
}

List<Sinf> sinfFromJson(String str) => List<Sinf>.from(json.decode(str).map((x) => Sinf.fromMap(x)));

String sinfToJson(List<Sinf> data) => json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Sinf {
    Sinf({
        this.id,
        this.title,
    });

    String id;
    String title;

    factory Sinf.fromMap(Map<String, dynamic> json) => Sinf(
        id: json["id"],
        title: json["title"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
    };
}
