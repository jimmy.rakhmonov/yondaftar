import 'dart:io';

import 'package:path_provider/path_provider.dart';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:yondaftar/api/models/models.dart';


class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  bool isFavorite = false;

  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TestDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Client ("
          "id Text,"
          "title TEXT,"
          "sinf_title TEXT,"
          "fan_title Text,"
          "pdf Text,"
          "pdf_preview TEXT"
          ")");
      await db.execute("CREATE TABLE Posts ("
          "id Text,"
          "title TEXT,"
          "text TEXT,"
          "images Text,"
          "video Text,"
          "fan_title Text,"
          "sinf_title Text,"
          "pubdate Text,"
          "views TEXT,"
          "markaz_title TEXT,"
          "markaz_logo TEXT"
          ")");
    });
  }

  newFav(Kitob newClient) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id) as id FROM Client");
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Client (id,title,sinf_title,fan_title,pdf,pdf_preview)"
        " VALUES (?,?,?,?,?,?)",
        [
          newClient.id,
          newClient.title,
          newClient.sinfTitle,
          newClient.fanTitle,
          newClient.pdf,
          newClient.pdfPreview
        ]);
    return raw;
  }

  newFavPost(Posts newClient) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id) as id FROM Posts");
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Posts (id,title,text,images,video,fan_title,sinf_title,pubdate,views,markaz_title,markaz_logo)"
        " VALUES (?,?,?,?,?,?,?,?,?,?,?)",
        [
          newClient.id,
          newClient.title,
          newClient.text,
          newClient.images,
          newClient.video,
          newClient.fanTitle,
          newClient.sinfTitle,
          newClient.pubdate,
          newClient.views,
          newClient.markazTitle,
          newClient.markazLogo
        ]);
    return raw;
  }

  updateClient(Kitob newClient) async {
    final db = await database;
    var res = await db.update("Client", newClient.toMap(),
        where: "id = ?", whereArgs: [newClient.id]);
    return res;
  }

  getClient(String id) async {
    final db = await database;
    var res = await db.query("Client", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Kitob.fromMap(res.first) : null;
  }

  Future<List<Kitob>> getAllClients() async {
    final db = await database;
    var res = await db.query("Client");
    List<Kitob> list =
        res.isNotEmpty ? res.map((c) => Kitob.fromMap(c)).toList() : [];
    return list;
  }

  getPost(String id) async {
    final db = await database;
    var res = await db.query("Posts", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Posts.fromMap(res.first) : null;
  }

  Future<List<Posts>> getAllPosts() async {
    final db = await database;
    var res = await db.query("Posts");
    List<Posts> list =
        res.isNotEmpty ? res.map((c) => Posts.fromMap(c)).toList() : [];
    return list;
  }

  deleteClient(String id) async {
    final db = await database;
    return db.delete("Client", where: "id = ?", whereArgs: [id]);
  }

  deletePost(String id) async {
    final db = await database;
    return db.delete("Posts", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from Client");
  }
}
