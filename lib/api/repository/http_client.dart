import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yondaftar/api/models/models.dart';

class Services2 {
  static const String baseurl = 'https://yondaftar.uz/api';

  static Future postViews(String id, String views) async {
    String postApi = baseurl + '/updateView';

    await http.post(postApi, body: {'id': id, 'views': views});

  }

  static Future<List<Kitob>> getFilteredBooks(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered2Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered3Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered4Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered5Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered6Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered7Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> getFiltered8Books(
      String idSinf, String idFan) async {
    String bookApi =
        baseurl + '/kitoblar?id_sinf=' + idSinf + '&id_fan=' + idFan;
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Kitob>> get getUsers async {
    const String bookApi = '${baseurl}/kitoblar';
    try {
      final response = await http.get(bookApi);
      if (response.statusCode == 200) {
        List<Kitob> list = parseUsers(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Kitob> parseUsers(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Kitob>((json) => Kitob.fromMap(json)).toList();
  }

  static Future<List<Fanlar>> get fetchFan async {
    const String fanApi = '${baseurl}/fanlar';

    /*String fileName = "pathString.json";
  var dir = await getTemporaryDirectory();

  File file = File(dir.path + "/" + fileName);

  return Posts.fromMap(response.body);*/

    try {
      var response = await http.get(fanApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getFan = fanlarFromJson(jsonData);
      return getFan;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Fanlar> parseFan(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Fanlar>((json) => Fanlar.fromMap(json)).toList();
  }

  static Future<List<Posts>> getFilteredPosts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getSecondFilteredPosts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getFiltered3Posts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getFiltered4Posts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getFiltered5Posts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getFiltered6Posts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getFiltered7Posts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> getFiltered8Posts(
      String idSinf, String idFan) async {
    String postApi = baseurl + '/posts?id_sinf=' + idSinf + '&id_fan=' + idFan;

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Posts>> get fetchPosts async {
    /*String fileName = "pathString.json";
  var dir = await getTemporaryDirectory();

  File file = File(dir.path + "/" + fileName);

  return Posts.fromMap(response.body);*/
    const String postApi = '${baseurl}/posts';

    try {
      var response = await http.get(postApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getPost = postsFromJson(jsonData);
      return getPost;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Posts> parsePosts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Posts>((json) => Posts.fromMap(json)).toList();
  }

  static Future<List<Markaz>> get fetchMarkaz async {
    const String markazApi = '${baseurl}/markaz';

    try {
      var response = await http.get(markazApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getMarkaz = markazFromJson(jsonData);
      return getMarkaz;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Markaz> parseMarkaz(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Markaz>((json) => Markaz.fromMap(json)).toList();
  }

  static Future<List<Sinf>> get fetchSinf async {
    const String sinfApi = '${baseurl}/markaz';

    try {
      var response = await http.get(sinfApi);

      final String jsonData = utf8.decode(response.bodyBytes);
      final getSinf = sinfFromJson(jsonData);
      return getSinf;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Sinf> parseSinf(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Sinf>((json) => Sinf.fromMap(json)).toList();
  }

  static Future<bool> updateView(String id,String views) async {
    try {
      var response = await http.post(Uri.parse('https://yondaftar.uz/api/updateView'),
          body: {'id': id, 'views': views});
          print(views);
      print('Response body: ${response.body}');
    } finally {
      
    }
  }
}
